const Collection = require('../src/Collection')
let testInstance

describe('Collection', () => {
  beforeEach(() => {
    testInstance = new Collection([
      {id: 1, value: 'item 1'},
      {id: 2, value: 'item 2'},
      {id: 3, value: 'item 3'},
      {id: 4, value: 'item 4'},
      {id: 5, value: 'item 5'}
    ])
  })

  describe('Collection.constructor', () => {
    it('should be iterable', () => {
      expect(Collection.prototype[Symbol.iterator]).toBeDefined()
    })

    it('should handle a list of parameters', () => {
      expect(new Collection(1, 2, 3, 4).size).toBe(4)
    })

    it('should handle an array of parameters', () => {
      expect(new Collection([1, 2, 3, 4]).size).toBe(4)
    })
  })

  it('should have size getter', () => {
    expect(testInstance.size).toBeDefined()
  })

  it('should have first getter', () => {
    expect(new Collection().first).toBeUndefined()
    expect(new Collection(1, 2, 3).first).toBeDefined()
  })

  it('should have last getter', () => {
    expect(new Collection().last).toBeUndefined()
    expect(new Collection(1, 2, 3).last).toBeDefined()
  })

  describe('Collection.add', () => {
    it('should be implemented', () => {
      expect(Collection.prototype.hasOwnProperty('add')).toBe(true)
      expect(Collection.prototype.add).toBeInstanceOf(Function)
    })

    it('should add an element if is not in collection', () => {
      const {size} = testInstance
      testInstance.add(
        {id: 1234, value: 'item 1234'},
        {id: 1234, value: 'item 1234'},
      )
      expect(testInstance.size).toBe(size + 2)
    })

    it('should throw and error when given element is already in collection', () => {
      expect(() => {
        testInstance.add(testInstance[0])
      }).toThrow()
    })
  })

  describe('Collection.delete', () => {
    it('should be implemented', () => {
      expect(Collection.prototype.hasOwnProperty('delete')).toBe(true)
      expect(Collection.prototype.delete).toBeInstanceOf(Function)
    })

    it('should delete an element', () => {
      const {size} = testInstance
      testInstance.delete(testInstance.first)
      expect(testInstance.size).toBe(size - 1)
    })

    it('should throw and error when element is not found', () => {
      expect(() => {
        testInstance.delete({value: 'test'})
      }).toThrow()
    })
  })

  describe('Collection.replace', () => {
    it('should be implemented', () => {
      expect(Collection.prototype.hasOwnProperty('replace')).toBe(true)
      expect(Collection.prototype.replace).toBeInstanceOf(Function)
    })

    it('should throw and error when element is not found', () => {
      expect(() => {
        testInstance.replace({value: 'original'}, {value: 'replacement'})
      }).toThrow()
    })

    it('should replace properly an element', () => {
      const element = testInstance.first
      const replacement = 'replaced'
      const {size} = testInstance
      expect(testInstance.first).toBe(element)
      expect(testInstance.first).not.toBe(replacement)
      testInstance.replace(element, replacement)
      expect(testInstance.first).toBe(replacement)
      expect(testInstance.first).not.toBe(element)
      expect(testInstance.size).toBe(size)
    })
  })

  describe('Collection.has', () => {
    it('Collection.has method should be implemented', () => {
      expect(Collection.prototype.hasOwnProperty('has')).toBe(true)
      expect(Collection.prototype.has).toBeInstanceOf(Function)
    })

    it('should check if an element is contained in collection', () => {
      const inCollection = testInstance[0]
      const notInCollection = {id: null}
      expect(testInstance.has(inCollection)).toBe(true)
      expect(testInstance.has(notInCollection)).toBe(false)
    })
  })

  describe('Collection.without', () => {
    it('without should be implemented', () => {
      expect(Collection.prototype.hasOwnProperty('without')).toBe(true)
      expect(Collection.prototype.without).toBeInstanceOf(Function)
    })
    
    it('should return collection without element', () => {
      const el = testInstance[0]
      expect(testInstance.without(el).has(el)).toBe(false)
    })
  })

})
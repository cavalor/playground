const {range} = require('../src/utils')

describe('range', () => {
  it('should return a Set', () => {
    expect(range()).toBeInstanceOf(Set)
  })

  it('return range from 0 when it\'s called with 1 parameter', () => {
    expect(range(4).size).toBe(5)
  })

  it('should return range contained into lower limit and upper limit', () => {
    const testRange = range(4, 5)
    expect(testRange.size).toBe(2)
    expect(testRange.has(4)).toBeTruthy()
    expect(testRange.has(5)).toBeTruthy()
  })

  it('should return range with lower limit, upper limit and step constraint', () => {
    const testRange = range(8, 10, 2)
    expect(testRange.size).toBe(2)
    expect(testRange.has(8)).toBeTruthy()
    expect(testRange.has(9)).toBeFalsy()
    expect(testRange.has(10)).toBeTruthy()
  })
})
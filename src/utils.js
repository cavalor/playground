exports.range = function range (...args) {
  const range = new Set()
  const firstIndex = args.length > 1 ? args[0] : 0
  const lastIndex = args.length > 1 ? args[1] : args[0]
  const step = args.length === 3 ? args[2] : 1

  for (let i = firstIndex; i <= lastIndex; i += step) {
    range.add(i)
  }

  return range
}
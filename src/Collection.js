class Collection extends Array {
  constructor (...args) {
    if (Array.isArray(args[0])) {
      super(...args[0])
    } else {
      super(...args)
    }
  }

  get size () {
    return this.length
  }

  get first () {
    return this[0]
  }

  get last () {
    return this[this.length - 1]
  }

  has (value) {
    return this.indexOf(value) !== -1
  }

  add (...values) {
    for (const value of values) {
      if (this.has(value)) {
        throw Error('Element already exist in collection')
      }
    }
    
    this.push(...values)
  }

  replace (element, replacement) {
    const index = this.indexOf(element)

    if (index === -1) {
      throw Error('Element not found')
    }

    this[index] = replacement
  }

  delete (value) {
    const index = this.indexOf(value)

    if (index === -1) {
      throw Error('Element not found')
    }

    this.splice(index, 1)
  }

  without(value) {
    return this.filter(v => v !== value)
  }
}

module.exports = Collection
